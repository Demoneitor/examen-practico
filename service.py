from flask import Flask, request,jsonify
from flask_mysqldb import MySQL
import secrets #a falta de conocimiento del JWT se utilizo secrets
from cryptography.fernet import Fernet

#Conexion MySQL
app = Flask(__name__)
app.config['MYSQL_HOST']='localhost'
app.config['MYSQL_USER']='root'
app.config['MYSQL_PASSWORD']=''
app.config['MYSQL_DB']='libreria'
mysql = MySQL(app)

@app.route('/') 
def create_user(): 
    return 'Root'

@app.route('/auth/register',methods = ['POST'])
def createuser():

    if request.method == 'POST':
        name = request.form['name']
        email = request.form['email']
        password = request.form['password']
        key = Fernet.generate_key() 
        with open("secret.key", "wb") as key_file:
         key_file.write(key)
        key = open("secret.key", "rb").read()
        print(key)
        encoded_message = password.encode()
        f = Fernet(key)
        encrypted_message = f.encrypt(encoded_message)
        if name == "" or email == "" or password == "":
            return 'Error'
        cur = mysql.connection.cursor()
        cur.execute('INSERT INTO users (name,email,password) VALUES (%s,%s,%s)',
        (name,email,encrypted_message))
        mysql.connection.commit()
        cur.close()
        return 'True'

def desen(valor):
    cifrado = valor
    cifradob = cifrado.encode()
    key = open("secret.key", "rb").read()
    f = Fernet(key)
    decryptd_message = f.decrypt(cifradob)
        
    return decryptd_message

@app.route('/auth/login',methods = ['POST'])
def loginUser():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']
        cur = mysql.connection.cursor()
        retorno = cur.execute("SELECT email,password FROM users WHERE email=%s",[email]) 
        if retorno > 0:
            rows = cur.fetchall()
            valor = ','.join(rows[0])
            data = valor.split(",")
            retorno = desen(data[1])

            
            if (email == data[0]) and (password.encode() == retorno):
                return 'True'
            else:
                return 'Error'
        else:
            return "Error" 

@app.route('/books',methods = ['GET','POST'])
def showBooks():
    
    if request.method == 'GET':
        cur = mysql.connection.cursor()
        cur.execute('SELECT * FROM books')
        data = cur.fetchall()
        print(data)
        return jsonify(data)
    
    if request.method == 'POST':
        author = request.form['author']
        isbn = request.form['isbn'] 
        release_date = request.form['release_date']
        title = request.form['title']
        cur = mysql.connection.cursor()
        result = cur.execute('SELECT isbn FROM books WHERE isbn=%s',[isbn])
        if result>0 or author == "" or isbn == "" or release_date == "" or title == "":
            return 'Error'
        cur.execute('INSERT INTO books (author,isbn,release_date,title,user_id,users_id) VALUES (%s,%s,%s,%s,2,2)',
        (author,isbn,release_date,title))
        mysql.connection.commit()
        cur.execute('SELECT id FROM books WHERE isbn=%s',[isbn]) 
        identi = cur.fetchall()
        cur.close()
        return jsonify(identi)

@app.route('/books/<string:id>',methods = ['DELETE'])
def deletebook(id):
    
    if request.method == 'DELETE':
        cur = mysql.connection.cursor()
        result = cur.execute("DELETE FROM books WHERE id=%s", [id])
        mysql.connection.commit()
        cur.close()
        if result > 0:
            return 'True'
        else:
            return 'Error'

if __name__ == '__main__': 
    app.run(debug=True)



    

