CREATE DATABASE IF NOT EXISTS Libreria;
USE Libreria;

CREATE TABLE books (
    author VARCHAR(255),
    id int (255) AUTO_INCREMENT,
    isbn VARCHAR(255) UNIQUE,
    release_date DATETIME,
    title VARCHAR(500),
    user_id int,
    users_id int(255),
    PRIMARY KEY(id)
);
CREATE INDEX users_id ON books(users_id);

CREATE TABLE users (
    email VARCHAR(255) UNIQUE,
    id INT(255) UNIQUE AUTO_INCREMENT,
    name VARCHAR(255),
    password VARCHAR(255),
    PRIMARY KEY(id)
);